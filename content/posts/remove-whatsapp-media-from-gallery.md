+++
showonlyimage = true
draft = false
writer = "Christian Danscheid"
date = "2018-10-30T18:17:00+01:00"
title = "Remove WhatsApp Media from your phone's gallery"
categories = ["android"]
tags = ["whatsapp", "tutorial"]
weight = 1
+++

I do not like the fact that WhatsApp by default clutters my Android phone's media gallery with every image or video it receives. If you find this annoying as well, here's how to fix it (source: [WhatsApp FAQ](https://faq.whatsapp.com/en/android/30031687)). It works fine on my Android 7 device but I do not know whether or not this works on IOS as well.

## Disable WhatsApp media in gallery
1. You will need access to Android's file system with a file explorer or alternatively using your computer's file explorer.
2. Go to *internal storage* → *WhatsApp* → *Media* → *WhatsApp Images*
3. Create a file named `.nomedia` (make sure this is the complete name, it will not work with **.nomedia.txt**; if Windows refuses the name, try `.nomedia.`)
4. Repeat the same step for *internal storage* → *WhatsApp* → *Media* → *WhatsApp Video*

Your gallery will now be free of WhatsApp media and you can focus on your own content!
