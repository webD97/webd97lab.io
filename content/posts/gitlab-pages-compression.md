+++
showonlyimage = true
draft = false
image = "img/gitlab-pages-compression/title-image.png"
writer = "Christian Danscheid"
date = "2018-10-19T22:02:00+02:00"
title = "Using GZIP compression on GitLab Pages"
categories = ["gitlab"]
tags = ["gitlab-pages", "tutorial"]
weight = 1
+++

While playing around with this new website, I noticed relatively long loading times. By default, GitLab Pages does not seem to use any compression when serving the static files of the website. Let's confirm this using a quick `curl` call:

```shell
chris@CHRISTIANS:~$ curl -ILH "Accept-Encoding: gzip" https://webd97.de
```
```http
HTTP/2 200
accept-ranges: bytes
cache-control: max-age=600
content-type: text/html; charset=utf-8
expires: Fri, 19 Oct 2018 20:18:08 UTC
last-modified: Fri, 19 Oct 2018 19:56:45 GMT
vary: Origin
content-length: 12034
date: Fri, 19 Oct 2018 20:08:08 GMT
```

If you take a closer look at the response headers, you can see that there is no `Content-Encoding` header although *gzip* was specified as an accepted encoding in the request and the response body is indeed not compressed.

## Compression support in GitLab Pages
There does not seem to be any official documentation on compression yet, but [an issue in the GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce/issues/15037) shows that it is supported. The catch is that the files need to be precompressed. This means that while GitLab Pages can serve compressed files, it cannot compress them *on the fly*.

So how does it work? Let's assume we have the following project structure deployed on GitLab Pages:

```
.
├── img
│   └── some_image.png
└── index.html
```

GitLab Pages will of course serve these files if requested, but if there is a compressed version next to it (and the client requests compression), the compressed version will be used. The compressed version of a file must be placed next to it:

```
.
├── img
│   ├── some_image.png
│   └── some_image.png.gz
├── index.html
└── index.html.gz
```

With this setup, the compressed files will be used whenever possible.

## Adding compression to the GitLab Pages build
To enable compression, we need to create static compressed files. Since we are working with static files, it is no problem at all to provide compressed variants of these files. We just need to call `gzip` for each file. For this website, I implemented this by calling `gzip -k -6 $(find public -type f)` after the successful Hugo build. The `k`flags keeps the input files, `6` is the compression level (1-9). Unfortunately, the `r` (recursive) flag is not supported by the base image, so I had to use a workaround here: `find public -type f` lists all files in the *public* directory (created by Hugo) recursively, which are then passed to gzip.

This is what my `.gitlab-ci.yml` looks like:
```yml
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo && gzip -k -6 $(find public -type f)
  artifacts:
    paths:
    - public
  only:
  - master
```

## Conclusion
Yes, compression of static files in GitLab Pages is possible and the required work is minimal. Go ahead and try it. The reduced file sizes helps a lot with page performance, especially on mobile devices, where bandwith is often limited.
