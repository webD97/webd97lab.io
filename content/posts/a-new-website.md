+++
showonlyimage = true
draft = false
writer = "Christian Danscheid"
date = "2018-10-14T16:13:33+02:00"
title = "A new website"
categories = []
tags = ["announcement"]
weight = 1
+++

Having discovered that my current hosting plan will become more expensive in December, I decided that my current setup with [WordPress](https://wordpress.org) is somewhat overkill, so I terminated my hosting contract with [1&1](https://1und1.de) and switched over to [GitLab](https://about.gitlab.com)'s static site hosting. This website is now powered by [Hugo](https://gohugo.io/) and [GitLab Pages](https://about.gitlab.com/features/pages/) with a TLS certificate issued by [Let's Encrypt](https://letsencrypt.org/).
