+++
showonlyimage = true
draft = false
image = "img/pipe-cmd-output-to-clipboard/title-image.png"
writer = "Christian Danscheid"
date = "2018-10-30T19:03:00+01:00"
title = "Pipe output of CMD or WSL to Windows clipboard"
categories = ["windows", "wsl"]
tags = ["tutorial"]
weight = 1
+++

In Windows, it is possible to copy the output from a command in cmd.exe directly to the clipboard, without requiring any mouse or other interaction. This can be helpful if you need to copy the output of larger commands, like `ipconfig` to a forum or another place, where redirection into a file using the `>` or `>>` operators might not be optimal. Here's how it works:

## Piping output to the clipboard
The command we need to use is `C:\Windows\System32\clip.exe`. Since its path is within the `PATH` environment variable, we can shorten this to just `clip`.

```batch
C:\Users\chris\Desktop> ipconfig | clip 
```

The output of `ipconfig` will then be copied to clipboard instead of printing it the console.

## Piping output from WSL to the clipboard
*Windows Subsystem for Linux* (WSL) is an amazing way to use a Linux within Windows without requiring a virtual machine. Due to the way it works, it can also execute a Windows program from within a WSL bash. Fortunately, the *PATH* variable of Windows is part of WSL's *PATH*, so we can pipe to clip.exe using:

```shell
ifconfig | clip.exe
```

In case Windows's *PATH* is not available in your WSL, you can find clip.exe at `/mnt/c/Windows/System32/clip.exe` because your *C:* is mounted as `/mnt/c/`

### Making life easier
If you plan to do this more frequently, always entering the complete filename *clip.exe* might be somewhat tedious, even with *TAB*-autocomplete. In this case, I recommend creating an alias, for example within `~/.bashrc`:

```shell
echo "alias clip=clip.exe" >> ~/.bashrc
```

This allows you to call clip.exe as `clip`. Also, don't forget that you can always use the `tee` command to both copy the output to the clipboard and still show it on the console:

[![ifconfig | tee clip](/img/pipe-cmd-output-to-clipboard/title-image.png)](/img/pipe-cmd-output-to-clipboard/title-image.png)