+++
date = "2018-10-14"
title = "About this blog"
tags = ["me"]
+++

The internet is such a gigantic space and I really appreciate that you somehow
landed on my website. Welcome! 🥳

This page primarily focuses on my journey across the cloud-native landscape: Cool projects,
interesting lessons-learned and whatever comes to my mind. I am absolutely convinced that
cloud-native software and platform engineering enable us to deliver even better software
products that make the end-users' lives better.

While many people associate the _cloud_ primarily with the well-known hyperscalers out there
(GCP, AWS, Azure ...), the true power of cloud-nativeness lies in the open source nature of
the amazing technology that enables cloud-stacks in the first place. Take a look at the
[CNCF landscape](https://landscape.cncf.io/) for instance: There are so many amazing projects
that allow __anyone__ to benefit from cloud technology, regardless of whether their stack
runs on a hyperscaler, a local data center or may be even a homelab.

Have fun and stay curious!

## A few words about me
For those wondering who I am: My name is Christian Danscheid and I am a software/platform
engineer at [REWE digital GmbH](https://rewe-digital.com/) in Cologne, Germany 🇩🇪. I have been
using and developing cloud-native software and platforms for many years now and I am still
not tired of. There is still so much more to explore!
